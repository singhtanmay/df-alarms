#
Required packages:
1. aws cli
2. Python 3
3. jq

Steps to use the script:
1. Make sure aws cli is installed in your local and is configured to pointing to correct environment.
2. Run the python file with below command

python lambda_alarms.py

3. It will generate the TF files for lambda alarms under the folder tf_files
4. Switch directory and go under the tf_files folder. Run terraform commands from here as required (plan or apply)
