######################## Alarms for RDS  ########################
######################### Deadlock ########################
resource "aws_cloudwatch_metric_alarm" "alarm-rds-content-deadlock" {
  alarm_name                = "alarm-rds-content-deadlock"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/RDS"
  metric_name               = "Deadlocks"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "1"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-content"
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm-rds-users-deadlock" {
  alarm_name                = "alarm-rds-users-deadlock"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/RDS"
  metric_name               = "Deadlocks"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "1"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-users"
  }
}

######################### Number of connection ########################
resource "aws_cloudwatch_metric_alarm" "alarm-rds-content-connections" {
  alarm_name                = "alarm-rds-content-connections"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "DatabaseConnections"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "1000"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-content"
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm-rds-users-connections" {
  alarm_name                = "alarm-rds-users-connections"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "DatabaseConnections"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "1000"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-users"
  }
}
######################### Write latency in RDS ########################

resource "aws_cloudwatch_metric_alarm" "alarm-rds-content-wr-latency" {
  alarm_name                = "alarm-rds-content-wr-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "WriteLatency"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "3"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-content"
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm-rds-users-wr-latency" {
  alarm_name                = "alarm-rds-users-wr-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "WriteLatency"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "3"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-users"
  }
}

######################### Commit latency in RDS ########################
resource "aws_cloudwatch_metric_alarm" "alarm-rds-content-commit-latency" {
  alarm_name                = "alarm-rds-content-commit-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "CommitLatency"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "3"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-content"
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm-rds-users-commit-latency" {
  alarm_name                = "alarm-rds-users-commit-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/RDS"
  metric_name               = "CommitLatency"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "3"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    DBClusterIdentifier = "rds-users"
  }
}

######################## Alarms for WAF  ########################
######################### Blocked request ########################

resource "aws_cloudwatch_metric_alarm" "alarm-waf-block-req" {
  alarm_name                = "alarm-waf-block-req"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "WAF"
  metric_name               = "BlockedRequests"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "25"
  insufficient_data_actions = []
  alarm_actions     = "${[var.sns_topic]}"

dimensions = {
    WebACL = "webacl"
    Region = "eu-west-1"
    RuleGroup = "FortinetAWSAPIGatewayRuleset"
  }
}