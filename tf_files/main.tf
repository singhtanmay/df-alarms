provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

variable "sns_topic"{
  default = "arn:aws:sns:eu-west-1:294038525052:ops-df-alarms"
}
