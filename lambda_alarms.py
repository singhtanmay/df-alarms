import sys
import subprocess
import json
from pprint import pprint

def execute_cmd(cmd):
    res = subprocess.run(
        cmd, capture_output=True, shell=True, check=True
    ).stdout.decode("utf-8")
    return res

# Get the list of lambda on which alert needs to be applied

res = execute_cmd("""
aws lambda list-functions | jq .Functions[].FunctionName
""")

lambda_list = res.split("\n")

lambda_names = []

for lam in lambda_list:
    no_quotes = lam.replace('"','')
    if not no_quotes.startswith("lmfn-ops-") and not no_quotes == "":
        lambda_names.append(no_quotes)

#lambda_names.discard("")
print(len(lambda_names))

################################################################################################################

# Template for Alarm

template_aws_cloudwatch_alarm = """
resource "aws_cloudwatch_metric_alarm" "alarm-{lmfn}-errors" {{
  alarm_name                = "alarm-{lmfn}-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  namespace                 = "AWS/Lambda"
  metric_name               = "Errors"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "5"
  insufficient_data_actions = []
  alarm_actions     = "${{[var.sns_topic]}}"

dimensions = {{
    FunctionName = "{lmfn}"
  }}
}}
"""
################################################################################################################

# Running the loop for generating tf files as per the existing lambda list

for specific_lambda in lambda_names:
    with open(f"./tf_files/__alarm-{specific_lambda}-error.tf", "w") as f:
        f.write(template_aws_cloudwatch_alarm.format(lmfn = specific_lambda))




















